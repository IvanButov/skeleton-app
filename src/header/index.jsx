import React from 'react'
import iconLogo from '../img/ae06b28302.png'
import { ProductContext } from '../context/product'
import { Link } from 'react-router-dom'
import './style.scss'

const Header = () => {
  const { product, setProduct } = React.useContext(ProductContext)
  return (
    <header id="header" className="header-section">
      <div className="container">
        <nav className="navbar ">
          <a href="index.html" className="navbar-brand">
            <img src={iconLogo} style={{ height: '70px' }} alt="Saasbiz" />
          </a>
          <div className="d-flex menu-wrap">
            <div id="mainmenu" className="mainmenu">
              <ul className="nav">
                <li>
                  <a data-scroll className="nav-link active" href="#home">
                    Home<span className="sr-only">(current)</span>
                  </a>
                  <ul>
                    <li>
                      <a href="index.html">Home 01</a>
                    </li>
                    <li>
                      <a href="index-2.html">Home 02</a>
                    </li>
                    <li>
                      <a href="index-3.html">Home 03</a>
                    </li>
                    <li>
                      <a href="index-4.html">Home 04</a>
                    </li>
                  </ul>
                </li>
                {/* <li>
                  <a data-scroll className="nav-link" href="#feature">
                    Features
                  </a>
                </li>
                <li>
                  <a data-scroll className="nav-link" href="#screenshots">
                    Screenshots
                  </a>
                </li>
                <li>
                  <a data-scroll className="nav-link" href="#reviews">
                    Reviews
                  </a>
                </li>
                <li>
                  <a data-scroll className="nav-link" href="#pricing">
                    Pricing
                  </a>
                </li> */}
                {/* <li>
                  <a href="#">Pages</a>
                  <ul>
                    <li>
                      <a href="about.html">About</a>
                    </li>
                    <li>
                      <a href="features.html">Features</a>
                    </li>
                    <li>
                      <a href="blog-grid.html">Blog Grid</a>
                    </li>
                    <li>
                      <a href="blog-classic.html">Blog classNameic</a>
                    </li>
                    <li>
                      <a href="blog-single.html">Blog Single</a>
                    </li>
                  </ul>
                </li> */}
                <li>
                  {/* <a href="contact.html">Contact</a> */}
                  <Link to={'/paid'} className={'text-showcase'}>
                    {' '}
                    Корзина
                  </Link>{' '}
                  <span className={'showcase-total text-showcase'}>
                    {product.length} шт.
                  </span>
                </li>
              </ul>
            </div>
            <div className="header-right">
              <Link to={'/login'} className="menu-btn anim-btn">
                Get Started<span></span>
              </Link>
            </div>
          </div>
        </nav>
      </div>
    </header>
  )
}

export default Header
