import React from 'react'
import { useContext } from 'react'
import { ProductContext } from '../context/product'
import Table from '@mui/material/Table'
import TableBody from '@mui/material/TableBody'
import TableCell from '@mui/material/TableCell'
import TableContainer from '@mui/material/TableContainer'
import TableHead from '@mui/material/TableHead'
import TableRow from '@mui/material/TableRow'
import Paper from '@mui/material/Paper'
import DeleteIcon from '@mui/icons-material/Delete'
import { deleteProduct } from '../showcase/card'

const PaidPage = () => {
  const { product, setProduct } = useContext(ProductContext)
  return (
    <div className={'container mt-5 pt-5'}>
      <div className={'container__row mt-5'}>
        <TableContainer component={Paper}>
          <Table sx={{ minWidth: 650 }} aria-label="simple table">
            <TableHead>
              <TableRow>
                <TableCell></TableCell>
                <TableCell>Наименование</TableCell>
                <TableCell align={'center'}>Описание</TableCell>
                <TableCell align={'center'}>Стоимость</TableCell>
                <TableCell></TableCell>
              </TableRow>
            </TableHead>
            <TableBody>
              {product.map(row => (
                <TableRow
                  key={row.id}
                  sx={{ '&:last-child td, &:last-child th': { border: 0 } }}
                >
                  <TableCell component="th" scope="row">
                    <img
                      src={row.images}
                      style={{ width: 100, borderRadius: 90 }}
                    />
                  </TableCell>
                  <TableCell>{row.title}</TableCell>
                  <TableCell>{row.desc}</TableCell>
                  <TableCell>{row.price}</TableCell>
                  <TableCell>
                    <DeleteIcon
                      onClick={() => deleteProduct(product, row.id, setProduct)}
                      style={{ cursor: 'pointer' }}
                      fontSize={'large'}
                    />
                  </TableCell>
                </TableRow>
              ))}
            </TableBody>
          </Table>
        </TableContainer>
      </div>
      <div className={'container__row mt-5'}>
        {product.length ? (
          <button
            style={{
              background: 'red',
              padding: '10px 20px',
              borderRadius: 8,
              color: 'white',
              fontWeight: 'bold',
              cursor: 'pointer',
            }}
            onClick={() => setProduct([])}
          >
            Очистить корзину
          </button>
        ) : (
          ''
        )}
      </div>
    </div>
  )
}

export default PaidPage
