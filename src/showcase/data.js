export const data = [
  {
    id: 0,
    images: 'https://mui.com/static/images/cards/contemplative-reptile.jpg',
    title: 'test',
    price: 4500,
    desc:
      'Lizards are a widespread group of squamate reptiles, with over 6,000 species, ranging across all continents except Antarctica',
  },
  {
    id: 1,
    images: 'https://mui.com/static/images/cards/contemplative-reptile.jpg',
    title: 'test',
    price: 1040,
    desc:
      'Lizards are a widespread group of squamate reptiles, with over 6,000 species, ranging across all continents except Antarctica',
  },
  {
    id: 2,
    images: 'https://mui.com/static/images/cards/contemplative-reptile.jpg',
    title: 'test',
    price: 2000,
    desc:
      'Lizards are a widespread group of squamate reptiles, with over 6,000 species, ranging across all continents except Antarctica',
  },
  {
    id: 3,
    images: 'https://mui.com/static/images/cards/contemplative-reptile.jpg',
    title: 'test',
    price: 1000,
    desc:
      'Lizards are a widespread group of squamate reptiles, with over 6,000 species, ranging across all continents except Antarctica',
  },
]
