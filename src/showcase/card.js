import React from 'react'
import Card from '@mui/material/Card'
import CardActions from '@mui/material/CardActions'
import CardContent from '@mui/material/CardContent'
import CardMedia from '@mui/material/CardMedia'
import Button from '@mui/material/Button'
import Typography from '@mui/material/Typography'

export function deleteProduct(product, id, setProduct) {
  const newList = product.filter(item => item.id !== id)

  return setProduct(newList)
}

export const CardProduct = ({
  title,
  desc,
  id,
  images,
  price,
  setProduct,
  product,
  status,
}) => {
  function addedProduct(newProduct) {
    if (status) {
      return deleteProduct(product, id, setProduct)
    }
    return setProduct(prev => [...prev, newProduct])
  }

  return (
    <div>
      <Card sx={{ maxWidth: 345 }}>
        <CardMedia
          component="img"
          height="140"
          image={images}
          alt="green iguana"
        />
        <CardContent>
          <Typography gutterBottom variant="h5" component="div">
            {title + ' ' + id}
          </Typography>
          <Typography variant="body2" color="text.secondary">
            {desc}
          </Typography>
        </CardContent>
        <CardActions>
          <Button
            size="small"
            onClick={() => addedProduct({ title, desc, id, images, price })}
          >
            {product.filter(item => item.id === id).length
              ? 'Удалить'
              : 'Купить' + ' ' + price}
          </Button>
        </CardActions>
      </Card>
    </div>
  )
}
