import React, { useContext } from 'react'
import { data } from './data'
import './style.scss'
import { CardProduct } from './card'
import { ProductContext } from '../context/product'

const ShowcasePage = () => {
  const { product, setProduct } = useContext(ProductContext)
  console.log('hello', product)
  return (
    <div className={'container'}>
      <div className={'container_row'}>
        <div className={'showcase'}>
          {data.map(item => (
            <CardProduct
              status={!!product.filter(pr => pr.id === item.id).length}
              product={product}
              setProduct={setProduct}
              {...item}
            />
          ))}
        </div>
      </div>
    </div>
  )
}

export default ShowcasePage
