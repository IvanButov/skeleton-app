import React, { Component } from 'react'
import './App.css'
import {} from 'bootstrap-4-react'
import { BrowserRouter as Router, Switch, Route } from 'react-router-dom'
import { withRouter } from 'react-router-dom'
import {} from 'bootstrap-4-react'
import './css/style.scss'
import Header from './header'
import HomePage from './home-page'
import ShowcasePage from './showcase'
import { AuthContext } from './context/auth'
import { ProductContext } from './context/product'
import PaidPage from './paid'
import Auth from './auth'

const App = props => {
  const [isAuth, setIsAuth] = React.useState(
    localStorage.getItem('isAuth') || false
  )

  // <AuthContext.Provider value={{ isAuth, setIsAuth }}> все что обернуто в данный компонент имет возможность const {isAuth, setIsAuth} = useContext(AuthContext) смотреть пример на странице auth/auth.jsx
  const [product, setProduct] = React.useState(
    JSON.parse(localStorage.getItem('product')) || []
  )

  React.useEffect(() => {
    localStorage.setItem('product', JSON.stringify(product))
  }, [product.length])

  return (
    <AuthContext.Provider value={{ isAuth, setIsAuth }}>
      <ProductContext.Provider value={{ product, setProduct }}>
        <Router history={props.history}>
          <Header />
          <Switch>
            <Route path="/showcase" component={ShowcasePage} />
            <Route path="/paid" component={PaidPage} />
            <Route path={'/login'} component={Auth} />
            <Route path="/" component={HomePage} />
          </Switch>
          {/* Место для Footer */}
        </Router>
      </ProductContext.Provider>
    </AuthContext.Provider>
  )
}

export default withRouter(App)
