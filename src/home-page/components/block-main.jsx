import React from 'react'

const BlockMain = () => {
  return (
    <section id="home" className="hero-section d-flex align-items-center">
      <div className="anim-elements">
        <div className="anim-element"></div>
        <div className="anim-element"></div>
        <div className="anim-element"></div>
        <div className="anim-element"></div>
        <div className="anim-element"></div>
      </div>
      <div className="container pos-rel">
        <div className="row hero-wrap d-flex align-items-center">
          <div className="col-lg-6 col-md-8 sm-padding">
            <div className="hero-content">
              <h4>
                <i className="fa fa-check"></i>#Начали работать с 2020
              </h4>
              <h1>
                Разработка сервисов и
                <br />
                интернет-магазинов
              </h1>
              <p>
                Создаем сайты и приложения с нуля. Развиваем уже существующие.
                Переводим задачи бизнеса в интерфейсы и API.
              </p>
              <div className="btn-group">
                <a className="default-btn" href="about.html">
                  Хотите узнать лучше?<span></span>
                </a>
                <a
                  href="https://youtu.be/bS5P_LAqiVg"
                  className="play-btn lightbox"
                  data-vbtype="video"
                >
                  <i className="fas fa-play"></i>Кто мы такие?
                </a>
              </div>
            </div>
          </div>
        </div>
        <div className="hero-moc"></div>
      </div>
    </section>
  )
}

export default BlockMain
