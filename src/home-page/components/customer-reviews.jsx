import React from 'react'
import IconPeople1 from '../../img/testi-1.jpg'
import IconPeople2 from '../../img/testi-2.jpg'
import IconPeople3 from '../../img/testi-3.jpg'
import IconPeople4 from '../../img/testi-4.jpg'
import Carousel from 'react-multi-carousel'
import 'react-multi-carousel/lib/styles.css'

const responsive = {
  superLargeDesktop: {
    breakpoint: { max: 4000, min: 3000 },
    items: 5,
  },
  desktop: {
    breakpoint: { max: 3000, min: 1024 },
    items: 3,
  },
  tablet: {
    breakpoint: { max: 1024, min: 464 },
    items: 2,
  },
  mobile: {
    breakpoint: { max: 464, min: 0 },
    items: 1,
  },
}

const CustomerReviews = () => {
  return (
    <section id="reviews" className="testimonial-section padding">
      <div className="bg-shape-left"></div>
      <div className="container">
        <div className="section-heading mb-40 text-center">
          <h2>Что о нас говорят клиенты?</h2>
          {/* <p>
            We provide marketing services to startups and small businesses to
            looking for a partner <br />
            of their digital media, design &amp; development, lead generation.
          </p> */}
        </div>
        <div
          style={{ marginTop: '30px' }}
          id="testimonial-carousel"
          className="testimonial-carousel owl-carousel"
        >
          <Carousel responsive={responsive}>
            <div className="testimonial-item">
              <div className="testi-thumb">
                <img src={IconPeople1} alt="img" />
              </div>
              <div className="author-details">
                <div className="author-info">
                  <h3>
                    Fiorella Ibáñez<span>Designer</span>
                  </h3>
                </div>
                <ul className="rating">
                  <li>
                    <i className="fa fa-star"></i>
                  </li>
                  <li>
                    <i className="fa fa-star"></i>
                  </li>
                  <li>
                    <i className="fa fa-star"></i>
                  </li>
                  <li>
                    <i className="fa fa-star"></i>
                  </li>
                  <li>
                    <i className="fa fa-star"></i>
                  </li>
                </ul>
              </div>
              <p>
                We provide marketing services startups and small businesses to
                looking for a partner of their digital media, design development
                lead generation.
              </p>
            </div>
            <div className="testimonial-item">
              <div className="testi-thumb">
                <img src={IconPeople2} alt="img" />
              </div>
              <div className="author-details">
                <div className="author-info">
                  <h3>
                    Eldie Goldey<span>Developer</span>
                  </h3>
                </div>
                <ul className="rating">
                  <li>
                    <i className="fa fa-star"></i>
                  </li>
                  <li>
                    <i className="fa fa-star"></i>
                  </li>
                  <li>
                    <i className="fa fa-star"></i>
                  </li>
                  <li>
                    <i className="fa fa-star"></i>
                  </li>
                  <li>
                    <i className="fa fa-star"></i>
                  </li>
                </ul>
              </div>
              <p>
                We provide marketing services startups and small businesses to
                looking for a partner of their digital media, design development
                lead generation.
              </p>
            </div>
            <div className="testimonial-item">
              <div className="testi-thumb">
                <img src={IconPeople3} alt="img" />
              </div>
              <div className="author-details">
                <div className="author-info">
                  <h3>
                    Kyle Frederick<span>Marketer</span>
                  </h3>
                </div>
                <ul className="rating">
                  <li>
                    <i className="fa fa-star"></i>
                  </li>
                  <li>
                    <i className="fa fa-star"></i>
                  </li>
                  <li>
                    <i className="fa fa-star"></i>
                  </li>
                  <li>
                    <i className="fa fa-star"></i>
                  </li>
                  <li>
                    <i className="fa fa-star"></i>
                  </li>
                </ul>
              </div>
              <p>
                We provide marketing services startups and small businesses to
                looking for a partner of their digital media, design development
                lead generation.
              </p>
            </div>
            <div className="testimonial-item">
              <div className="testi-thumb">
                <img src={IconPeople4} alt="img" />
              </div>
              <div className="author-details">
                <div className="author-info">
                  <h3>
                    Peter Scezency<span>Designer</span>
                  </h3>
                </div>
                <ul className="rating">
                  <li>
                    <i className="fa fa-star"></i>
                  </li>
                  <li>
                    <i className="fa fa-star"></i>
                  </li>
                  <li>
                    <i className="fa fa-star"></i>
                  </li>
                  <li>
                    <i className="fa fa-star"></i>
                  </li>
                  <li>
                    <i className="fa fa-star"></i>
                  </li>
                </ul>
              </div>
              <p>
                We provide marketing services startups and small businesses to
                looking for a partner of their digital media, design development
                lead generation.
              </p>
            </div>
          </Carousel>
        </div>
      </div>
    </section>
  )
}

export default CustomerReviews
