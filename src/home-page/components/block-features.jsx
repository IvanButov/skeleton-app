import React from 'react'

const BlockFeatures = ({ classBlock, classColor, title, desc }) => {
  return (
    <div className="features-item">
      <i className={classBlock}></i>
      <h3 className={classColor}>{title}</h3>
      <p>{desc}</p>
    </div>
  )
}
export default BlockFeatures
