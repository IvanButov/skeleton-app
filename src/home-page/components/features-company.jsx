import React from 'react'
import BlockFeatures from './block-features'

const FeaturesCompany = () => {
  return (
    <section id="feature" className="features-section padding">
      <div className="container">
        <div className="section-heading text-center mb-40">
          <h2>BSP-Digital функции</h2>
          <p>
            Превращаем набор требований и пожеланий в удобный продукт, который{' '}
            <br />
            успешно развивается и растет с нашей поддержкой, зарабатывает деньги{' '}
            <br />и выигрывает конкурсы.
          </p>
        </div>
        <div className="row">
          <div className="col-md-4 col-sm-6 sm-padding">
            <BlockFeatures
              title={'Аналитика рынка'}
              classColor={'color-red'}
              classBlock={'truno truno-chart color-red bg-red'}
              desc={`Мы проводим анализ рынка, определяем необходимые требования для вашего проекта.`}
            />
          </div>
          <div className="col-md-4 col-sm-6 sm-padding">
            <BlockFeatures
              title={'Маркетинг решения'}
              classColor={'color-blue'}
              classBlock={'truno truno-network color-blue bg-blue'}
              desc={`Подбираем комплекс маркетинговых мероприятий, создаем готовое решение под ваши требования.`}
            />
          </div>
          <div className="col-md-4 col-sm-6 sm-padding">
            <BlockFeatures
              title={'Кастомное решение'}
              classColor={'color-green'}
              classBlock={'truno truno-hours color-green bg-green'}
              desc={`Для каждого проекта создаем уникальное решение, готовим уникальный дизайн и выполняем брендирование проекта.`}
            />
          </div>
        </div>
      </div>
    </section>
  )
}

export default FeaturesCompany
